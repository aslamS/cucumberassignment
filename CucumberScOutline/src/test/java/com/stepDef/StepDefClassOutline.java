package com.stepDef;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefClassOutline {
	static WebDriver driver = null;

	@Given("^open the browser$")
	public void open_the_browser() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

	}

	@Then("the homepage of website is get displayed")
	public void the_homepage_of_website_is_get_displayed() {

		String title = driver.getTitle();
		System.out.println(title);

	}

	@Then("user have to click the login button")
	public void user_have_to_click_the_login_button() {
		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
	}

	@When("user have to enter valid {string} and {string} in textbox")
	public void user_have_to_enter_valid_and_in_textbox(String string, String string2) {
		driver.findElement(By.id("Email")).sendKeys(string);
		driver.findElement(By.id("Password")).sendKeys(string2);
	}

	@Then("user have to click login button")
	public void user_have_to_click_login_button() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("user have to taken homepage of Demo Web Shop")
	public void user_have_to_taken_homepage_of_demo_web_shop() {
//		String text = driver.findElement(By.linkText("aslamsardhar98@gmail.com")).getText();
//		System.out.println(text);
		System.out.println("demo web shop");
	}

	@Then("user have to click logout button")
	public void user_have_to_click_logout_button() {
		driver.findElement(By.xpath("//a[text()='Log out']")).click();
	}

	@Then("close the browser")
	public void close_the_browser() throws InterruptedException {
		Thread.sleep(2000);
		driver.quit();
	}
}
